import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import * as constantes from './../constantes';

@Injectable()
export class PettyInicialService {
    constructor(private http: Http, private router: Router,) {}
    modelo = "/petty-inicial";
    
    private getHeaders(): Headers {
      let token = localStorage.getItem('jwt');
      let headers = new Headers({
          'Content-Type': 'application/json',
      });
      return headers;
    }

    public registrarPettyInicial(pettyInicial:any, pettyDetalle: any[]): Promise<any> {
      return this.http.post(
        constantes.urlServidor + this.modelo,
        {pettyInicial, pettyDetalle},
        { headers: this.getHeaders() })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public listarPettyInicialPorUsuario(usuarioId: number): Promise<any[]> {
        return this.http.get(
        constantes.urlServidor + this.modelo + '/usuario',
        { headers: this.getHeaders(), params: {usuarioId:usuarioId} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public listarPettyInicialAperturadoPorUsuario(usuarioId: number): Promise<any[]> {
        return this.http.get(
        constantes.urlServidor + this.modelo + '/aperturado/usuario',
        { headers: this.getHeaders(), params: {usuarioId:usuarioId} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public listarPettyInicialCerrado(fechaInicial: string, fechaFinal:string): Promise<any[]> {
        return this.http.get(
        constantes.urlServidor + this.modelo + '/cerrado',
        { headers: this.getHeaders(), params: {fechaInicial:fechaInicial, fechaFinal:fechaFinal} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public exportarExcelPagosPorMaquina(id: number): Promise<any> {
        return this.http.get(
        constantes.urlServidor + this.modelo + '/exportar-excel',
        { headers: this.getHeaders(), 
        params: {id:id} })
        .toPromise()
        .then((data) => data)
        .catch((error) => console.log(error));
    }
    
}
