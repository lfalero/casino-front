import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import * as constantes from './../constantes';

@Injectable()
export class PagosService {
    constructor(private http: Http, private router: Router,) {}
    modelo = "/pagos";
    
    private getHeaders(): Headers {
      let token = localStorage.getItem('jwt');
      let headers = new Headers({
          'Content-Type': 'application/json',
      });
      return headers;
    }

    public listarTotalPorTurno(pettyInicialId: number): Promise<any[]> {
        return this.http.get(
        constantes.urlServidor + this.modelo + '/total',
        { headers: this.getHeaders(), params: {pettyInicialId:pettyInicialId} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public listarTotalPorMaquina(maquinaId: number, offset: number, limit: number, fechaInicial: string, fechaFinal:string): Promise<any[]> {
        return this.http.get(
        constantes.urlServidor + this.modelo + '/maquina',
        { headers: this.getHeaders(), 
        params: {maquinaId:maquinaId, offset:offset, limit:limit, fechaInicial:fechaInicial, fechaFinal:fechaFinal} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public listarPagosPorUsuario(usuarioId: number): Promise<any[]> {
        return this.http.get(
        constantes.urlServidor + this.modelo + '/usuario',
        { headers: this.getHeaders(), 
        params: {usuarioId:usuarioId} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public listarPagosPorUsuarioAperturado(usuarioId: number, offset: number, limit: number): Promise<any[]> {
        return this.http.get(
        constantes.urlServidor + this.modelo + '/usuario-aperturado',
        { headers: this.getHeaders(), 
        params: {usuarioId:usuarioId, offset:offset, limit:limit} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public exportarExcelPagosPorMaquina(maquinaId: number, fechaInicial: string, fechaFinal:string): Promise<any> {
        return this.http.get(
        constantes.urlServidor + this.modelo + '/exportar-excel',
        { headers: this.getHeaders(), 
        params: {maquinaId:maquinaId, fechaInicial:fechaInicial, fechaFinal:fechaFinal} })
        .toPromise()
        .then((data) => data)
        .catch((error) => console.log(error));
    }

    public aprobarPagos(id: number): Promise<any[]> {
        return this.http.get(
        constantes.urlServidor + this.modelo + '/aprobar',
        { headers: this.getHeaders(), 
        params: {id:id} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public confirmarPagos(id: number): Promise<any[]> {
        return this.http.get(
        constantes.urlServidor + this.modelo + '/confirmar',
        { headers: this.getHeaders(), 
        params: {id:id} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public cantidadPagosRegistrados(usuarioId: number): Promise<any[]> {
        return this.http.get(
        constantes.urlServidor + this.modelo + '/cantidad-registrados',
        { headers: this.getHeaders(), 
        params: {usuarioId:usuarioId} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }
}


