import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import * as constantes from './../constantes';

@Injectable()
export class UsuarioService {
    constructor(private http: Http, private router: Router,) {}
    modelo = "/usuario";
    
    private getHeaders(): Headers {
      let token = localStorage.getItem('jwt');
      let headers = new Headers({
          'Content-Type': 'application/json',
      });
      return headers;
    }

    public iniciarSesion(usuario:any): Promise<any> {
        return this.http.post(
        constantes.urlServidor + this.modelo + '/iniciar-sesion',
        usuario,
        { headers: this.getHeaders() })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public cerrarSesion(): Promise<any[]> {
        return this.http.get(
        constantes.urlServidor + '/cerrar-sesion',
        { headers: this.getHeaders(), params: {} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public registrarUsuario(usuario:any): Promise<any> {
        return this.http.post(
        constantes.urlServidor + this.modelo, usuario,
        { headers: this.getHeaders() })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public listarUsuarioFiltro(offset: number, limit: number): Promise<any[]> {
        return this.http.get(
        constantes.urlServidor + this.modelo + '/filtro',
        { headers: this.getHeaders(), params: {offset:offset, limit:limit} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public detalleUsuario(id: number): Promise<any[]> {
        return this.http.get(
        constantes.urlServidor  + this.modelo + '/' + id,
        { headers: this.getHeaders(), params: {} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public modificarUsuario(id: number, usuario: any): Promise<any> {
        return this.http.put(
        constantes.urlServidor + this.modelo + '/' +id,
        usuario,
        { headers: this.getHeaders(), params: {} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public eliminarUsuario(id: number): Promise<any> {
        return this.http.delete(
        constantes.urlServidor + this.modelo + '/' +id,
        { headers: this.getHeaders(), params: {} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public aprobarUsuario(id: number): Promise<any> {
        return this.http.get(
        constantes.urlServidor + this.modelo + '/aprobar',
        { headers: this.getHeaders(), params: {id: id} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }
}
