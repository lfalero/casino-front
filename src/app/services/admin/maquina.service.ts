import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import * as constantes from './../constantes';

@Injectable()
export class MaquinaService {
    constructor(private http: Http, private router: Router,) {}
    modelo = "/maquina";
    
    private getHeaders(): Headers {
      let token = localStorage.getItem('jwt');
      let headers = new Headers({
          'Content-Type': 'application/json',
      });
      return headers;
    }

    public registrarMaquina(maquina:any): Promise<any> {
      return this.http.post(
        constantes.urlServidor + this.modelo, maquina,
        { headers: this.getHeaders() })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public listarMaquinaFiltro(offset: number, limit: number): Promise<any[]> {
        return this.http.get(
        constantes.urlServidor + this.modelo + '/filtro',
        { headers: this.getHeaders(), params: {offset:offset, limit:limit} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public detalleMaquina(id: number): Promise<any[]> {
        return this.http.get(
          constantes.urlServidor  + this.modelo + '/' + id,
          { headers: this.getHeaders(), params: {} })
          .toPromise()
          .then((data) => data.json())
          .catch((error) => console.log(error));
    }

    public modificarMaquina(id: number, maquina: any): Promise<any> {
        return this.http.put(
        constantes.urlServidor + this.modelo + '/' +id,
        maquina,
        { headers: this.getHeaders(), params: {} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public eliminarMaquina(id: number): Promise<any> {
        return this.http.delete(
        constantes.urlServidor + this.modelo + '/' +id,
        { headers: this.getHeaders(), params: {} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public aprobarMaquina(id: number): Promise<any> {
        return this.http.get(
        constantes.urlServidor + this.modelo + '/aprobar',
        { headers: this.getHeaders(), params: {id: id} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }
}
