import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import * as constantes from './../constantes';

@Injectable()
export class BilleteService {
    constructor(private http: Http) {}
    modelo = "/billete";

    private getHeaders(): Headers {
      let token = localStorage.getItem('jwt');
      let headers = new Headers({
          'Content-Type': 'application/json',
          'Authorization': token,
      });
      return headers;
    }

    public listarBillete(): Promise<any[]> {
        return this.http.get(
        constantes.urlServidor + this.modelo,
        { headers: this.getHeaders(), params: {} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

}
