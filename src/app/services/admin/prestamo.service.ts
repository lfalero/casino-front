import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import * as constantes from './../constantes';

@Injectable()
export class PrestamoService {
    constructor(private http: Http, private router: Router,) {}
    modelo = "/prestamo";
    
    private getHeaders(): Headers {
      let token = localStorage.getItem('jwt');
      let headers = new Headers({
          'Content-Type': 'application/json',
      });
      return headers;
    }

    public registrarPrestamo(prestamo:any, prestamoDetalle: any[]): Promise<any> {
      return this.http.post(
        constantes.urlServidor + this.modelo,
        {prestamo, prestamoDetalle},
        { headers: this.getHeaders() })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }

    public listarPrestamoPorPettyInicial(pettyInicialId: number): Promise<any[]> {
        return this.http.get(
        constantes.urlServidor + this.modelo + '/petty-inicial',
        { headers: this.getHeaders(), params: {pettyInicialId:pettyInicialId} })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }
}
