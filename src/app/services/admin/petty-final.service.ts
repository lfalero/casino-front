import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import * as constantes from './../constantes';

@Injectable()
export class PettyFinalService {
    constructor(private http: Http, private router: Router,) {}
    modelo = "/petty-final";
    
    private getHeaders(): Headers {
      let token = localStorage.getItem('jwt');
      let headers = new Headers({
          'Content-Type': 'application/json',
      });
      return headers;
    }

    public registrarPettyFinal(pettyFinal:any, pettyDetalle: any[]): Promise<any> {
      return this.http.post(
        constantes.urlServidor + this.modelo,
        {pettyFinal, pettyDetalle},
        { headers: this.getHeaders() })
        .toPromise()
        .then((data) => data.json())
        .catch((error) => console.log(error));
    }
}
