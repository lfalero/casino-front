import { Route, RouterModule } from '@angular/router';
import { PettyFinalComponent } from './petty-final.component';
import { ModuleWithProviders } from '@angular/core';

//COMPONENTES
import { PettyFinalRegistrarComponent } from './registrar/petty-final-registrar.component';

let routes: Route[] = [
    {
        path: '',
        component: PettyFinalComponent,
        children: [
            { path: 'nuevo/:id',  component: PettyFinalRegistrarComponent },
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
