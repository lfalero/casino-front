import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {PageEvent} from '@angular/material';
import { Title } from '@angular/platform-browser';
import {Location} from '@angular/common';

//SERVICES
import { InicioService } from '../../../services/admin/inicio.service';
import { SettingsService } from '../../../services/settings.service';
import { UsuarioService } from '../../../services/admin/usuario.service';
import { BilleteService } from '../../../services/admin/billete.service';
import { PettyInicialService } from '../../../services/admin/petty-inicial.service';
import { PettyFinalService } from '../../../services/admin/petty-final.service';
import { PagosService } from '../../../services/admin/pagos.service';

@Component({
  selector: 'app-petty-final-registrar',
  templateUrl: './petty-final-registrar.component.html',
  styleUrls: ['./petty-final-registrar.component.scss']
})
export class PettyFinalRegistrarComponent implements OnInit {

  pettyInicialId: number = 0;
  montoPettyInicial: number = 0;
  montoBillete: number = 0;
  montoGeneral: number = 0;
  pettyFinal: any = {pettyFinal:0, tPagos:0, tGastos:0, cuadreFinal:0, diferencia: 0};
  billetes: any[] = [];
  pettyDetalle: any[] = [];
  loading: boolean = true;
  vistaPantalla: boolean = false;
  position: string = 'above';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private inicioService: InicioService,
    private settingsService: SettingsService,
    private title: Title,
    private location: Location,
    private usuarioService: UsuarioService,
    private billeteService: BilleteService,
    private pettyInicialService: PettyInicialService,
    private pettyFinalService: PettyFinalService,
    private pagosService: PagosService) { }
    

    ngOnInit() {
      var rolId = parseInt(localStorage.getItem("rolId"));
      if(rolId != 2) this.router.navigate(['sistema/inicio']);
      
      this.title.setTitle("Cerrar Caja");
      this.route.params.subscribe(params => {
        this.pettyInicialId = params['id'];
      });
      this.listarPettyInicialAperturado();
      
    }

    listarPettyInicialAperturado(){
      let usuarioId = parseInt(localStorage.getItem("id"));
      this.pettyInicialService.listarPettyInicialAperturadoPorUsuario(usuarioId).then((data)=>{
        if(data.length > 0){
          this.montoPettyInicial = data[0].montoPettyInicial;
          this.listarBillete();
          this.listarPagosTotal(this.pettyInicialId);
        }else{
          this.router.navigate(["sistema/petty-inicial/nuevo"]);
        }
      });
    }

    listarBillete(){
      this.billeteService.listarBillete().then((data)=>{
        this.billetes = data;
        for(let i=0; i<data.length; i++){
          this.pettyDetalle.push({
            montoBillete: data[i].monto,
            montoGeneral: 0,
            cantidad: 0,
            billeteId: data[i].id,
          }) 
        }
        this.calcularMonto();
        this.loading = false;
        this.vistaPantalla = true;
      });
    }

    listarPagosTotal(pettyInicialId: number){
      this.pagosService.listarTotalPorTurno(pettyInicialId).then((data: any)=>{
        this.pettyFinal.tPagos = data.pagoTotal;
        this.calcularMonto();
      });
    }

    valorCantidad(i: number, cantidad: string) {
      this.pettyDetalle[i].cantidad = parseInt(cantidad);
      this.calcularMonto();
    }

    valorCantidadGeneral(){
      this.calcularMonto();
    }

    calcularMonto(){
      this.montoBillete = 0;
      for(let i=0; i<this.pettyDetalle.length; i++){
        let cantidad = (this.pettyDetalle[i].cantidad == NaN) ? 0 : this.pettyDetalle[i].cantidad;
        this.montoBillete = this.montoBillete + (cantidad * this.pettyDetalle[i].montoBillete); 
      }
      this.montoBillete = this.montoBillete + this.montoGeneral;
      this.pettyFinal.cuadreFinal = this.montoPettyInicial - this.montoBillete;
      this.pettyFinal.diferencia = this.pettyFinal.cuadreFinal - this.pettyFinal.tPagos - this.pettyFinal.tGastos;
    }

    registrar(){
      if(this.montoBillete > 0){
        this.loading = true;
        this.pettyFinal.montoPettyFinal = this.montoBillete;
        this.pettyFinal.usuarioId = parseInt(localStorage.getItem("id"));
        this.pettyFinal.pettyInicialId = this.pettyInicialId;

        this.pettyDetalle.push({
          "montoBillete": 0,
          "montoGeneral": this.montoGeneral,
          "cantidad": 1,
          "billeteId": 0,
        });
        this.pettyFinalService.registrarPettyFinal(this.pettyFinal, this.pettyDetalle)
        .then((data: any) => {
          this.loading = false;
          if(data.state){
            this.settingsService.showNotification('top','right', "Registro Correcto", 2);
            this.router.navigate(["sistema/inicio"]);
          }
        });
      }else{
        this.settingsService.showNotification('top','right', "Seleccionar al menos un monto", 4);
      }
    }

    regresar(){
      this.location.back();
    }
}
