import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'modulo-petty-final',
    template: '<router-outlet></router-outlet>'
})
export class PettyFinalComponent {

    constructor() { }

}
