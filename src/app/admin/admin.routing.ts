import { Route,Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { ModuleWithProviders } from '@angular/core';

export const routes: Routes = [
    {
        path: 'sistema',
        component: AdminComponent,
        children: [
            { path: 'inicio', loadChildren: './inicio/inicio.module#InicioModule' },
            { path: 'petty-inicial', loadChildren: './petty-inicial/petty-inicial.module#PerttyInicialModule' },
            { path: 'petty-final', loadChildren: './petty-final/petty-final.module#PerttyFinalModule' },
            { path: 'maquina', loadChildren: './maquina/maquina.module#MaquinaModule' },
            { path: 'usuario', loadChildren: './usuario/usuario.module#UsuarioModule' },
            { path: 'reporte', loadChildren: './reporte/reporte.module#ReporteModule' },
            { path: 'pago-manual', loadChildren: './pago-manual/pago-manual.module#PagoManualModule' },
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
