import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'modulo-pago-manual',
    template: '<router-outlet></router-outlet>'
})
export class PagoManualComponent {

    constructor() { }

}
