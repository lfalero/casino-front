import { Route, RouterModule } from '@angular/router';
import { PagoManualComponent } from './pago-manual.component';
import { ModuleWithProviders } from '@angular/core';

//COMPONENTES
import { PagoManualListarComponent } from './listar/pago-manual-listar.component';

let routes: Route[] = [
    {
        path: '',
        component: PagoManualComponent,
        children: [
            { path: '',  component: PagoManualListarComponent },
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
