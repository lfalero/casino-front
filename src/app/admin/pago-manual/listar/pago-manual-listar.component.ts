import { Component, OnInit, OnDestroy} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {PageEvent} from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import * as constantes from './../../../services/constantes';

//TIMER
import {Subscription} from "rxjs";
import {TimerObservable} from "rxjs/observable/TimerObservable";

//SERVICES
import { InicioService } from '../../../services/admin/inicio.service';
import { SettingsService } from '../../../services/settings.service';
import { UsuarioService } from '../../../services/admin/usuario.service';
import { PagosService } from '../../../services/admin/pagos.service';

@Component({
  selector: 'app-pago-manual-listar',
  templateUrl: './pago-manual-listar.component.html',
  styleUrls: ['./pago-manual-listar.component.scss']
})
export class PagoManualListarComponent implements OnInit, OnDestroy {

  position: string = 'above';
  pagos: any[] = [];

  // MatPaginator Inputs
  pageSizeOptions: any[] = [10, 25, 50, 100];
  length: number = 0;
  offset: number = 0;
  limit: number = this.pageSizeOptions[0];
  loading: boolean = true;
  vistaPantalla: boolean = false;

  //Timer
  private subscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private inicioService: InicioService,
    private settingsService: SettingsService,
    private title: Title,
    private usuarioService: UsuarioService,
    private pagosService: PagosService,
    private formBuilder: FormBuilder) { }

    ngOnInit() {
      var rolId = parseInt(localStorage.getItem("rolId"));
      if(rolId != 2) this.router.navigate(['sistema/inicio']);

      this.title.setTitle("Pagos Manuales");
      this.vistaPantalla = true;
      this.listarPagos();

      //Inicializar Timer
      let tiempoInicial = constantes.timerSeconds.refresh * 1000;
      let tiempoSecuencial = constantes.timerSeconds.refresh * 1000;
      let timer = TimerObservable.create(tiempoInicial, tiempoSecuencial);
      this.subscription = timer.subscribe(t => {
        this.listarPagos();
      });
    }
  
    ngOnDestroy() {
      //Destruir Timer
      this.subscription.unsubscribe();
    }

    setPageSizeOptions(data) {
      this.offset = data.pageIndex * data.pageSize;
      this.limit = data.pageSize;
      this.listarPagos();
    }

    listarPagos(){
      this.loading = true;
      this.pagos = [];
      var usuarioId = parseInt(localStorage.getItem("id"));
      this.pagosService.listarPagosPorUsuarioAperturado(usuarioId, this.offset, this.limit).then((data: any) => {
        this.pagos = data;
        this.length = (data.length > 0) ? data[0].cantidadTotal : 0;
        this.loading = false;
      });
    }

    aprobarPagos(id: number){
      this.pagosService.aprobarPagos(id)
      .then((data) => {
        this.settingsService.showNotification('top','right', this.settingsService.mensaje.aprobar, 2);
        this.listarPagos();
      })
      .catch((error) => {
        this.settingsService.showNotification('top','right', error, 4);
      });
    }
}
