import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './pago-manual.routing';

import { MatButtonModule, MatRadioModule, MatInputModule, MatMenuModule, MatCheckboxModule, MatNativeDateModule } from '@angular/material';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import {MatExpansionModule} from '@angular/material/expansion';

//SERVICES
import { InicioService } from '../../services/admin/inicio.service';

//COMPONENTES
import { PagoManualComponent } from './pago-manual.component';
import { PagoManualListarComponent } from './listar/pago-manual-listar.component';

@NgModule({
    imports: [
        HttpModule,
        routing,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        MatButtonModule,
        MatRadioModule,
        MatInputModule,
        MatMenuModule,
        MatCheckboxModule,
        MatNativeDateModule,
        MatTooltipModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatSelectModule,
        MatDialogModule,
        MatExpansionModule,
    ],
    declarations: [
        PagoManualComponent,
        PagoManualListarComponent,
    ],
    entryComponents: [
    ],
    providers: [
        InicioService,
    ]
})

export class PagoManualModule { }
