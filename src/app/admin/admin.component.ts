import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
//TIMER
import {Subscription} from "rxjs";
import {TimerObservable} from "rxjs/observable/TimerObservable";
import * as constantes from './../services/constantes';

//SERVICES
import { SettingsService } from '../services/settings.service';
import { UsuarioService } from '../services/admin/usuario.service';
import { PagosService } from '../services/admin/pagos.service';

@Component({
    selector: 'modulo-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit, OnDestroy {
  public id: number;
  public backgroundColor: string;
  public bandera: boolean = false;
  public bandera_responsive: boolean = false;
  public toggoleShowHide: string = "block";
  public bandera_sesion: boolean = (localStorage.getItem("rolId") == null) ? true : false;
  
  @Input() title: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public settingService: SettingsService,
    private usuarioService: UsuarioService,
    private pagosService: PagosService) {
    this.id = settingService.getSidebarImageIndex() + 1;
    this.backgroundColor = settingService.getSidebarColor();
  }

  //Timer
  private subscription: Subscription;

  ngOnInit() {
    /*this.settingService.sidebarImageIndexUpdate.subscribe((id: number) => {
      this.id = id + 1;
    });
    this.settingService.sidebarColorUpdate.subscribe((color: string) => {
      this.backgroundColor = color;
    });*/

    //Inicializar Timer
    let tiempoInicial = constantes.timerSeconds.notification * 1000;
    let tiempoSecuencial = constantes.timerSeconds.notification * 1000;
    let timer = TimerObservable.create(tiempoInicial, tiempoSecuencial);
    this.subscription = timer.subscribe(t => {
      this.notificacionPagosAprobar();
    });
  }

  ngOnDestroy() {
    /*this.settingService.sidebarImageIndexUpdate.unsubscribe();
    this.settingService.sidebarColorUpdate.unsubscribe();*/

    //Destruir Timer
    this.subscription.unsubscribe();
  }

  menuClick() {
    this.bandera = !this.bandera;
    if(this.bandera){
      document.getElementById("bootstrap-collapse").classList.add('sidebar-mini');
    }else{
      document.getElementById("bootstrap-collapse").classList.remove('sidebar-mini');
    }
  }

  menuClickResponsive() {
    this.bandera_responsive = !this.bandera_responsive;
    if(this.bandera_responsive){
      document.getElementById("bootstrap-collapse").classList.add('nav-open');
      document.getElementById("sidebar").classList.add('sidebar-responsive-on');
      document.getElementById("sidebar").classList.remove('sidebar-responsive-off');
    }else{
      document.getElementById("bootstrap-collapse").classList.remove('nav-open');
      document.getElementById("sidebar").classList.add('sidebar-responsive-off');
      document.getElementById("sidebar").classList.remove('sidebar-responsive-on');
    }
  }

  cerrarSesion(){
    
    localStorage.removeItem('id');
    localStorage.removeItem('usuario');
    localStorage.removeItem('rolId');
    //this.router.navigate(["/"]);
    this.router.navigateByUrl('/iniciar-sesion');
    //location.href = '#/iniciar-sesion';
    
    /*this.usuarioService.cerrarSesion().then((data: any) => {
      //localStorage.removeItem('jwt');
      localStorage.removeItem('id');
      localStorage.removeItem('usuario');
      this.router.navigate(["iniciar-sesion"]);
    });*/
  }

  notificacionPagosAprobar(){
    var usuarioId = parseInt(localStorage.getItem("id"));
    this.pagosService.cantidadPagosRegistrados(usuarioId).then((data: any) => {
      if(data.cantidadTotal > 0){
        let mensaje = "Hay "+data.cantidadTotal+" pagos pendientes";
        this.settingService.showNotification('top','right', mensaje, 2);
      }
    });    
  }
}

