import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {PageEvent} from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';

//SERVICES
import { InicioService } from '../../../services/admin/inicio.service';
import { SettingsService } from '../../../services/settings.service';
import { UsuarioService } from '../../../services/admin/usuario.service';
import { MaquinaService } from '../../../services/admin/maquina.service';

@Component({
  selector: 'app-maquina-listar',
  templateUrl: './maquina-listar.component.html',
  styleUrls: ['./maquina-listar.component.scss']
})
export class MaquinaListarComponent implements OnInit {

  position: string = 'above';
  maquinas: any[] = [];

  // MatPaginator Inputs
  pageSizeOptions: any[] = [10, 25, 50, 100];
  length: number = 0;
  offset: number = 0;
  limit: number = this.pageSizeOptions[0];
  loading: boolean = true;
  vistaPantalla: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private inicioService: InicioService,
    private settingsService: SettingsService,
    private title: Title,
    private usuarioService: UsuarioService,
    private maquinaService: MaquinaService,
    private formBuilder: FormBuilder) { }

    ngOnInit() {
      var rolId = parseInt(localStorage.getItem("rolId"));
      if(rolId != 1) this.router.navigate(['sistema/inicio']);

      this.title.setTitle("Listar Máquina");
      this.vistaPantalla = true;
      this.listarMaquina();
    }

    setPageSizeOptions(data) {
      this.offset = data.pageIndex * data.pageSize;
      this.limit = data.pageSize;
      this.listarMaquina();
    }

    listarMaquina(){
      this.loading = true;
      this.maquinas = [];
      this.maquinaService.listarMaquinaFiltro(this.offset, this.limit).then((data: any) => {
        this.maquinas = data;
        this.length = data[0].cantidadTotal;
        this.loading = false;
      });
    }

    detalleMaquina(id: number){
      this.router.navigate(['sistema/maquina/', id]);
    }

    registrarMaquina(){
      this.router.navigate(['sistema/maquina/nuevo']);
    }

    eliminarMaquina(id: number){
      this.maquinaService.eliminarMaquina(id)
      .then((data) => {
        this.settingsService.showNotification('top','right', this.settingsService.mensaje.debaja, 2);
        this.listarMaquina();
      })
      .catch((error) => {
        this.settingsService.showNotification('top','right', error, 4);
      });
    }

    aprobarMaquina(id: number){
      this.maquinaService.aprobarMaquina(id)
      .then((data) => {
        this.settingsService.showNotification('top','right', this.settingsService.mensaje.aprobar, 2);
        this.listarMaquina();
      })
      .catch((error) => {
        this.settingsService.showNotification('top','right', error, 4);
      });
    }
}
