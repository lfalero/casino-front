import { Route, RouterModule } from '@angular/router';
import { MaquinaComponent } from './maquina.component';
import { ModuleWithProviders } from '@angular/core';

//COMPONENTES
import { MaquinaListarComponent } from './listar/maquina-listar.component';
import { MaquinaRegistrarComponent } from './registrar/maquina-registrar.component';
import { MaquinaModificarComponent } from './modificar/maquina-modificar.component';

let routes: Route[] = [
    {
        path: '',
        component: MaquinaComponent,
        children: [
            { path: '',  component: MaquinaListarComponent },
            { path: 'nuevo',  component: MaquinaRegistrarComponent },
            { path: ':id',  component: MaquinaModificarComponent },
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
