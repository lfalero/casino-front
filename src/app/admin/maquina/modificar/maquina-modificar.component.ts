import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {PageEvent} from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';

//SERVICES
import { InicioService } from '../../../services/admin/inicio.service';
import { SettingsService } from '../../../services/settings.service';
import { UsuarioService } from '../../../services/admin/usuario.service';
import { MaquinaService } from '../../../services/admin/maquina.service';

@Component({
  selector: 'app-maquina-modificar',
  templateUrl: './maquina-modificar.component.html',
  styleUrls: ['./maquina-modificar.component.scss']
})
export class MaquinaModificarComponent implements OnInit {

  id: number;
  maquina: any = {};
  loading: boolean = true;
  vistaPantalla: boolean = false;

  validator: any = {
    numero : new FormControl('', [Validators.required]),
    nombre : new FormControl('', [Validators.required]),
    denominacion : new FormControl('', [Validators.required]),
  };
  formGroup: FormGroup;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private inicioService: InicioService,
    private settingsService: SettingsService,
    private title: Title,
    private usuarioService: UsuarioService,
    private maquinaService: MaquinaService,
    private formBuilder: FormBuilder) { }

    getErrorMessage() {
      return {
        numero: this.validator.numero.hasError('required') ? 'Campo requerido': '',
        nombre: this.validator.nombre.hasError('required') ? 'Campo requerido': '',
        denominacion: this.validator.denominacion.hasError('required') ? 'Campo requerido': '',
      };
    }

    ngOnInit() {
      var rolId = parseInt(localStorage.getItem("rolId"));
      if(rolId != 1) this.router.navigate(['sistema/inicio']);

      this.title.setTitle("Modificar Máquina");
      this.formGroup = this.formBuilder.group(this.validator);
      
      this.route.params.subscribe(params => {
        this.id = params['id'];
      });

      this.optenerMaquina();
    }

    optenerMaquina(){
      this.maquinaService.detalleMaquina(this.id).then((data: any) => {
        this.maquina = data;
        this.loading = false;
        this.vistaPantalla = true;  
      });
    }
    
    onSubmit(){
      if(this.formGroup.valid){
        this.loading = true;
        this.maquinaService.modificarMaquina(this.id, this.maquina)
        .then((data) => {
          this.settingsService.showNotification('top','right', this.settingsService.mensaje.modificar, 2);
          this.router.navigate(['sistema/maquina']);
        })
        .catch((error) => {
          this.settingsService.showNotification('top','right', error, 4);
        })
      }
    }

    regresar(){
      this.router.navigate(['sistema/maquina']);
    }
}
