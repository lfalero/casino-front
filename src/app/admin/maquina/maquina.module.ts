import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './maquina.routing';

import { MatButtonModule, MatRadioModule, MatInputModule, MatMenuModule, MatCheckboxModule, MatNativeDateModule } from '@angular/material';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import {MatExpansionModule} from '@angular/material/expansion';

//SERVICES
import { InicioService } from '../../services/admin/inicio.service';

//COMPONENTES
import { MaquinaComponent } from './maquina.component';
import { MaquinaListarComponent } from './listar/maquina-listar.component';
import { MaquinaRegistrarComponent } from './registrar/maquina-registrar.component';
import { MaquinaModificarComponent } from './modificar/maquina-modificar.component';

@NgModule({
    imports: [
        HttpModule,
        routing,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        MatButtonModule,
        MatRadioModule,
        MatInputModule,
        MatMenuModule,
        MatCheckboxModule,
        MatNativeDateModule,
        MatTooltipModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatSelectModule,
        MatDialogModule,
        MatExpansionModule,
    ],
    declarations: [
        MaquinaComponent,
        MaquinaListarComponent,
        MaquinaRegistrarComponent,
        MaquinaModificarComponent,
    ],
    entryComponents: [
    ],
    providers: [
        InicioService,
    ]
})

export class MaquinaModule { }
