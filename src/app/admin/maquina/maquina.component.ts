import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'modulo-maquina',
    template: '<router-outlet></router-outlet>'
})
export class MaquinaComponent {

    constructor() { }

}
