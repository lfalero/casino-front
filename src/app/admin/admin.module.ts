import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './admin.routing';
import {ConfirmationPopoverModule} from 'angular-confirmation-popover';

import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './../app.routes';

//COMPONENTES
import { AdminComponent } from './admin.component';
import { MatButtonModule, MatRadioModule, MatInputModule, MatMenuModule, MatCheckboxModule, MatNativeDateModule } from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field';

import { SidebarComponent } from './../sidebar/sidebar.component';
import { NavbarComponent } from './../shared/navbar/navbar.component';

//MODULOS IMPORTADOS
import { InicioModule } from './../admin/inicio/inicio.module';
import { PerttyInicialModule } from './../admin/petty-inicial/petty-inicial.module';

//MODELAS
import { PrestamoModalRegistrarComponent } from './../admin/petty-inicial/aperturado/modals/prestamo-modal-registrar/prestamo-modal-registrar.component';

//SERVICES
import { UsuarioService } from '../services/admin/usuario.service';
import { PettyInicialService } from '../services/admin/petty-inicial.service';
import { PettyFinalService } from '../services/admin/petty-final.service';
import { PrestamoService } from '../services/admin/prestamo.service';
import { BilleteService } from '../services/admin/billete.service';
import { MaquinaService } from '../services/admin/maquina.service';
import { PagosService } from '../services/admin/pagos.service';

//MODALES

@NgModule({
    imports: [
        HttpModule,
        routing,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        MatButtonModule,
        MatRadioModule,
        MatInputModule,
        MatMenuModule,
        MatCheckboxModule,
        MatNativeDateModule,
        MatDatepickerModule,
        MatFormFieldModule,
        ConfirmationPopoverModule.forRoot({
            confirmButtonType: 'danger'
        }),
        InicioModule,
        PerttyInicialModule,
    ],
    declarations: [
        AdminComponent,
        SidebarComponent,
        NavbarComponent,
    ],
    entryComponents: [
        PrestamoModalRegistrarComponent,
    ],
    providers: [
        UsuarioService,
        PettyInicialService,
        PettyFinalService,
        PrestamoService,
        BilleteService,
        MaquinaService,
        PagosService,
    ]
})

export class AdminModule { }
