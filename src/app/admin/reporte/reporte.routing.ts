import { Route, RouterModule } from '@angular/router';
import { ReporteComponent } from './reporte.component';
import { ModuleWithProviders } from '@angular/core';

//COMPONENTES
import { PagosManualesComponent } from './pagos-manuales/pagos-manuales.component';
import { TurnoCerradoComponent } from './turno-cerrado/turno-cerrado.component';

let routes: Route[] = [
    {
        path: '',
        component: ReporteComponent,
        children: [
            { path: 'pagos-manuales',  component: PagosManualesComponent },
            { path: 'turno-cerrado',  component: TurnoCerradoComponent },
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
