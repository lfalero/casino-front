import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'modulo-reporte',
    template: '<router-outlet></router-outlet>'
})
export class ReporteComponent {

    constructor() { }

}
