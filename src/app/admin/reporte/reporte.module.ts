import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './reporte.routing';

import { MatButtonModule, MatRadioModule, MatInputModule, MatMenuModule, MatCheckboxModule, MatNativeDateModule } from '@angular/material';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDatepickerModule} from '@angular/material/datepicker';

//SERVICES
import { InicioService } from '../../services/admin/inicio.service';

//COMPONENTES
import { ReporteComponent } from './reporte.component';
import { PagosManualesComponent } from './pagos-manuales/pagos-manuales.component';
import { TurnoCerradoComponent } from './turno-cerrado/turno-cerrado.component';

@NgModule({
    imports: [
        HttpModule,
        routing,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        MatButtonModule,
        MatRadioModule,
        MatInputModule,
        MatMenuModule,
        MatCheckboxModule,
        MatNativeDateModule,
        MatTooltipModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatSelectModule,
        MatDialogModule,
        MatExpansionModule,
        MatDatepickerModule,
    ],
    declarations: [
        ReporteComponent,
        PagosManualesComponent,
        TurnoCerradoComponent,
    ],
    entryComponents: [
    ],
    providers: [
        InicioService,
    ]
})

export class ReporteModule { }
