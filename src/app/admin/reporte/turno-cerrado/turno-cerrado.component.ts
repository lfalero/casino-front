import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {PageEvent, DateAdapter} from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';

//SERVICES
import { InicioService } from '../../../services/admin/inicio.service';
import { SettingsService } from '../../../services/settings.service';
import { UsuarioService } from '../../../services/admin/usuario.service';
import { MaquinaService } from '../../../services/admin/maquina.service';
import { PagosService } from '../../../services/admin/pagos.service';
import { PettyInicialService } from '../../../services/admin/petty-inicial.service';

@Component({
  selector: 'app-turno-cerrado',
  templateUrl: './turno-cerrado.component.html',
  styleUrls: ['./turno-cerrado.component.scss']
})
export class TurnoCerradoComponent implements OnInit {

  position: string = 'above';
  maquinas: any[] = [];
  pagos: any[] = [];
  fechaInicial: any = new Date(new Date().setDate(new Date().getDate() - 2));
  fechaFinal: any = new Date();

  // MatPaginator Inputs
  pageSizeOptions: any[] = [10, 25, 50, 100];
  length: number = 0;
  offset: number = 0;
  limit: number = this.pageSizeOptions[0];
  loading: boolean = false;
  vistaPantalla: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private inicioService: InicioService,
    private settingsService: SettingsService,
    private title: Title,
    private usuarioService: UsuarioService,
    private maquinaService: MaquinaService,
    private pagosService: PagosService,
    private pettyInicialService: PettyInicialService,
    private dateAdapter:DateAdapter<Date>,
    private formBuilder: FormBuilder) {
      dateAdapter.setLocale('es'); // DD/MM/YYYY
    }

    ngOnInit() {
      var rolId = parseInt(localStorage.getItem("rolId"));
      if(rolId != 1) this.router.navigate(['sistema/inicio']);
      
      this.title.setTitle("Pagos Manuales");
      this.vistaPantalla = true;
      this.listarPagos();
    }

    setPageSizeOptions(data) {
      this.offset = data.pageIndex * data.pageSize;
      this.limit = data.pageSize;
      this.listarPagos();
    }


    listarPagos(){
      this.loading = true;
      this.pagos = [];
      this.pettyInicialService.listarPettyInicialCerrado(this.fechaInicial.toISOString().slice(0,10), this.fechaFinal.toISOString().slice(0,10)).then((data: any) => {
        if(data.length > 0){
          this.pagos = data;
          this.length = data[0].cantidadTotal;
        }
        this.loading = false;
      });
    }

    registrarMaquina(){
      this.router.navigate(['sistema/maquina/nuevo']);
    }

    exportarExcel(id){
      this.loading = true;
      this.pettyInicialService.exportarExcelPagosPorMaquina(id)
      .then((data: any) => {
        this.loading = false;
        location.href = data.url;
      });
    
    }
}
