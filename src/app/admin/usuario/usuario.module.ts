import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './usuario.routing';

import { MatButtonModule, MatRadioModule, MatInputModule, MatMenuModule, MatCheckboxModule, MatNativeDateModule } from '@angular/material';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import {MatExpansionModule} from '@angular/material/expansion';

//SERVICES
import { InicioService } from '../../services/admin/inicio.service';

//COMPONENTES
import { UsuarioComponent } from './usuario.component';
import { UsuarioListarComponent } from './listar/usuario-listar.component';
import { UsuarioRegistrarComponent } from './registrar/usuario-registrar.component';
import { UsuarioModificarComponent } from './modificar/usuario-modificar.component';

@NgModule({
    imports: [
        HttpModule,
        routing,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        MatButtonModule,
        MatRadioModule,
        MatInputModule,
        MatMenuModule,
        MatCheckboxModule,
        MatNativeDateModule,
        MatTooltipModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatSelectModule,
        MatDialogModule,
        MatExpansionModule,
    ],
    declarations: [
        UsuarioComponent,
        UsuarioListarComponent,
        UsuarioRegistrarComponent,
        UsuarioModificarComponent,
    ],
    entryComponents: [
    ],
    providers: [
        InicioService,
    ]
})

export class UsuarioModule { }
