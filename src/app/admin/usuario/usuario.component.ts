import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'modulo-usuario',
    template: '<router-outlet></router-outlet>'
})
export class UsuarioComponent {

    constructor() { }

}
