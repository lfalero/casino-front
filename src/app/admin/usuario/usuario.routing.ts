import { Route, RouterModule } from '@angular/router';
import { UsuarioComponent } from './usuario.component';
import { ModuleWithProviders } from '@angular/core';

//COMPONENTES
import { UsuarioListarComponent } from './listar/usuario-listar.component';
import { UsuarioRegistrarComponent } from './registrar/usuario-registrar.component';
import { UsuarioModificarComponent } from './modificar/usuario-modificar.component';

let routes: Route[] = [
    {
        path: '',
        component: UsuarioComponent,
        children: [
            { path: '',  component: UsuarioListarComponent },
            { path: 'nuevo',  component: UsuarioRegistrarComponent },
            { path: ':id',  component: UsuarioModificarComponent },
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
