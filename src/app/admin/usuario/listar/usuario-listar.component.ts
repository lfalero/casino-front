import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {PageEvent} from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';

//SERVICES
import { InicioService } from '../../../services/admin/inicio.service';
import { SettingsService } from '../../../services/settings.service';
import { UsuarioService } from '../../../services/admin/usuario.service';
import { MaquinaService } from '../../../services/admin/maquina.service';

@Component({
  selector: 'app-usuario-listar',
  templateUrl: './usuario-listar.component.html',
  styleUrls: ['./usuario-listar.component.scss']
})
export class UsuarioListarComponent implements OnInit {

  position: string = 'above';
  usuarios: any[] = [];

  // MatPaginator Inputs
  pageSizeOptions: any[] = [10, 25, 50, 100];
  length: number = 0;
  offset: number = 0;
  limit: number = this.pageSizeOptions[0];
  loading: boolean = true;
  vistaPantalla: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private inicioService: InicioService,
    private settingsService: SettingsService,
    private title: Title,
    private usuarioService: UsuarioService,
    private maquinaService: MaquinaService,
    private formBuilder: FormBuilder) { }

    ngOnInit() {
      var rolId = parseInt(localStorage.getItem("rolId"));
      if(rolId != 1) this.router.navigate(['sistema/inicio']);
      
      this.title.setTitle("Listar Usuario");
      this.vistaPantalla = true;
      this.listarUsuario();
    }

    setPageSizeOptions(data) {
      this.offset = data.pageIndex * data.pageSize;
      this.limit = data.pageSize;
      this.listarUsuario();
    }

    listarUsuario(){
      this.loading = true;
      this.usuarios = [];
      this.usuarioService.listarUsuarioFiltro(this.offset, this.limit).then((data: any) => {
        this.usuarios = data;
        this.length = data[0].cantidadTotal;
        this.loading = false;
      });
    }

    detalleUsuario(id: number){
      this.router.navigate(['sistema/usuario/', id]);
    }

    registrarUsuario(){
      this.router.navigate(['sistema/usuario/nuevo']);
    }

    eliminarUsuario(id: number){
      this.usuarioService.eliminarUsuario(id)
      .then((data) => {
        this.settingsService.showNotification('top','right', this.settingsService.mensaje.debaja, 2);
        this.listarUsuario();
      })
      .catch((error) => {
        this.settingsService.showNotification('top','right', error, 4);
      });
    }

    aprobarUsuario(id: number){
      this.usuarioService.aprobarUsuario(id)
      .then((data) => {
        this.settingsService.showNotification('top','right', this.settingsService.mensaje.aprobar, 2);
        this.listarUsuario();
      })
      .catch((error) => {
        this.settingsService.showNotification('top','right', error, 4);
      });
    }

}
