import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {PageEvent} from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';

//SERVICES
import { InicioService } from '../../../services/admin/inicio.service';
import { SettingsService } from '../../../services/settings.service';
import { UsuarioService } from '../../../services/admin/usuario.service';

@Component({
  selector: 'app-usuario-registrar',
  templateUrl: './usuario-registrar.component.html',
  styleUrls: ['./usuario-registrar.component.scss']
})
export class UsuarioRegistrarComponent implements OnInit {

  roles: any[] = [];
  usuario: any = {rolId: 0};
  loading: boolean = true;
  vistaPantalla: boolean = false;

  validator: any = {
    rolId : new FormControl('', [Validators.required]),
    nroCedula : new FormControl('', [Validators.required]),
    nombres : new FormControl('', [Validators.required]),
    apellidoPaterno : new FormControl('', [Validators.required]),
    apellidoMaterno : new FormControl('', [Validators.required]),
    correo : new FormControl('', [Validators.required]),
    celular : new FormControl('', [Validators.required]),
    usuario : new FormControl('', [Validators.required]),
    clave : new FormControl('', [Validators.required]),
  };
  formGroup: FormGroup;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private inicioService: InicioService,
    private settingsService: SettingsService,
    private title: Title,
    private usuarioService: UsuarioService,
    private formBuilder: FormBuilder) { }

    getErrorMessage() {
      return {
        rolId: this.validator.rolId.hasError('required') ? 'Campo requerido': '',
        nroCedula: this.validator.nroCedula.hasError('required') ? 'Campo requerido': '',
        nombres: this.validator.nombres.hasError('required') ? 'Campo requerido': '',
        apellidoPaterno: this.validator.apellidoPaterno.hasError('required') ? 'Campo requerido': '',
        apellidoMaterno: this.validator.apellidoMaterno.hasError('required') ? 'Campo requerido': '',
        correo: this.validator.correo.hasError('required') ? 'Campo requerido': '',
        celular: this.validator.celular.hasError('required') ? 'Campo requerido': '',
        usuario: this.validator.usuario.hasError('required') ? 'Campo requerido': '',
        clave: this.validator.clave.hasError('required') ? 'Campo requerido': '',
      };
    }

    ngOnInit() {
      var rolId = parseInt(localStorage.getItem("rolId"));
      if(rolId != 1) this.router.navigate(['sistema/inicio']);
      
      this.title.setTitle("Registrar Usuario");
      this.formGroup = this.formBuilder.group(this.validator);
      this.loading = false;
      this.vistaPantalla = true;

      this.roles.push({id:1, nombre: "Administrador"});
      this.roles.push({id:2, nombre: "Cajero"});
      this.roles.push({id:3, nombre: "Encargado de Pago Manual"});
    }
    
    onSubmit(){
      if(this.formGroup.valid){
        this.loading = true;
        this.usuarioService.registrarUsuario(this.usuario)
        .then((data) => {
          this.settingsService.showNotification('top','right', this.settingsService.mensaje.registrar, 2);
          this.router.navigate(['sistema/usuario']);
        })
        .catch((error) => {
          this.settingsService.showNotification('top','right', error, 4);
        })
      }
    }

    regresar(){
      this.router.navigate(['sistema/usuario']);
    }
}
