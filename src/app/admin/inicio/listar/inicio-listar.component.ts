import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {PageEvent} from '@angular/material';
import { Title } from '@angular/platform-browser';

//SERVICES
import { InicioService } from '../../../services/admin/inicio.service';
import { SettingsService } from '../../../services/settings.service';
import { UsuarioService } from '../../../services/admin/usuario.service';

@Component({
  selector: 'app-inicio-listar',
  templateUrl: './inicio-listar.component.html',
  styleUrls: ['./inicio-listar.component.scss']
})
export class InicioListarComponent implements OnInit {
  
  usuario: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private inicioService: InicioService,
    private settingsService: SettingsService,
    private title: Title,
    private usuarioService: UsuarioService) { }
    
    ngOnInit() {
      this.title.setTitle("Inicio");
      this.usuario = localStorage.getItem("usuario");
      /*this.inicioService.authorization().then((data: any) => {
        if(data.recordSet.length != 0){
          this.ngOnLoad();
        }else{
          this.router.navigate(["iniciar-sesion"]);
        }
      });*/
    }

    ngOnLoad(){
      
    }
}
