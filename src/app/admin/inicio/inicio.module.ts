import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './inicio.routing';

import { MatButtonModule, MatRadioModule, MatInputModule, MatMenuModule, MatCheckboxModule, MatNativeDateModule } from '@angular/material';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

//SERVICES
import { InicioService } from '../../services/admin/inicio.service';

//COMPONENTES
import { InicioComponent } from './inicio.component';
import { InicioListarComponent } from './listar/inicio-listar.component';

@NgModule({
    imports: [
        HttpModule,
        routing,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        MatButtonModule,
        MatRadioModule,
        MatInputModule,
        MatMenuModule,
        MatCheckboxModule,
        MatNativeDateModule,
        MatTooltipModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
    ],
    declarations: [
        InicioComponent,
        InicioListarComponent,
    ],
    entryComponents: [
    ],
    providers: [
        InicioService,
    ]
})

export class InicioModule { }
