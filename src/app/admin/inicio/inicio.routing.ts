import { Route, RouterModule } from '@angular/router';
import { InicioComponent } from './inicio.component';
import { ModuleWithProviders } from '@angular/core';

//COMPONENTES
import { InicioListarComponent } from './listar/inicio-listar.component';

let routes: Route[] = [
    {
        path: '',
        component: InicioComponent,
        children: [
            { path: '',  component: InicioListarComponent },
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
