import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'modulo-inicio',
    template: '<router-outlet></router-outlet>'
})
export class InicioComponent {

    constructor() { }

}
