import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './petty-inicial.routing';

import { MatButtonModule, MatRadioModule, MatInputModule, MatMenuModule, MatCheckboxModule, MatNativeDateModule } from '@angular/material';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import {MatExpansionModule} from '@angular/material/expansion';

//SERVICES
import { InicioService } from '../../services/admin/inicio.service';

//COMPONENTES
import { PettyInicialComponent } from './petty-inicial.component';
import { PettyInicialRegistrarComponent } from './registrar/petty-inicial-registrar.component';
import { PettyInicialAperturadoComponent } from './aperturado/petty-inicial-aperturado.component';
import { PrestamoModalRegistrarComponent } from './aperturado/modals/prestamo-modal-registrar/prestamo-modal-registrar.component';

@NgModule({
    imports: [
        HttpModule,
        routing,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        MatButtonModule,
        MatRadioModule,
        MatInputModule,
        MatMenuModule,
        MatCheckboxModule,
        MatNativeDateModule,
        MatTooltipModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatSelectModule,
        MatDialogModule,
        MatExpansionModule,
    ],
    declarations: [
        PettyInicialComponent,
        PettyInicialRegistrarComponent,
        PettyInicialAperturadoComponent,
        PrestamoModalRegistrarComponent,
    ],
    entryComponents: [
    ],
    providers: [
        InicioService,
    ]
})

export class PerttyInicialModule { }
