import { Component, OnInit, Inject} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

//SERVICES
import { SettingsService } from '../../../../../services/settings.service';
import { PettyInicialService } from '../../../../../services/admin/petty-inicial.service';
import { PrestamoService } from '../../../../../services/admin/prestamo.service';
import { BilleteService } from '../../../../../services/admin/billete.service';

@Component({
  selector: 'app-prestamo-modal-registrar',
  templateUrl: './prestamo-modal-registrar.component.html',
  styleUrls: ['./prestamo-modal-registrar.component.scss']
})
export class PrestamoModalRegistrarComponent implements OnInit {

  pettyInicialId: number = 0;
  montoBillete: number = 0;
  loading = true;
  vistaPantalla = false;
  prestamoDetalle: any[] = [];
  billetes: any[] = [];
  prestamo: any = {};

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private settingsService: SettingsService,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<PrestamoModalRegistrarComponent>,
    private fb: FormBuilder,
    private pettyInicialService: PettyInicialService,
    private prestamoService: PrestamoService,
    private billeteService: BilleteService) { }
    
    ngOnInit() {
      this.pettyInicialId = this.data.pettyInicialId;
      this.listarBillete();
    }

    listarBillete(){
      this.billeteService.listarBillete().then((data)=>{
        this.billetes = data;
        for(let i=0; i<data.length; i++){
          this.prestamoDetalle.push({
            montoBillete: data[i].monto,
            cantidad: 0,
            billeteId: data[i].id,
          }) 
        }
        this.loading = false;
        this.vistaPantalla = true;
      });
    }
    
    valorCantidad(i: number, cantidad: string) {
      this.prestamoDetalle[i].cantidad = parseInt(cantidad);
      this.calcularMonto();
    }

    calcularMonto(){
      this.montoBillete = 0;
      for(let i=0; i<this.prestamoDetalle.length; i++){
        let cantidad = (this.prestamoDetalle[i].cantidad == NaN) ? 0 : this.prestamoDetalle[i].cantidad;
        this.montoBillete = this.montoBillete + (cantidad * this.prestamoDetalle[i].montoBillete); 
      }
    }

    onSubmit() {
      this.loading = true;
      this.prestamo.total = this.montoBillete;
      this.prestamo.pettyInicialId = this.pettyInicialId;
      this.prestamoService.registrarPrestamo(this.prestamo, this.prestamoDetalle)
      .then((data: any) => {
        this.loading = false;
        if(data.state){
          this.settingsService.showNotification('top','right', "Registro Correcto", 2);
        }else{
          this.settingsService.showNotification('top','right', "Error al registrar", 4);
        }
        this.dialogRef.close(true);
      });
      
    }
}
