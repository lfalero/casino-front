import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {PageEvent} from '@angular/material';
import {MatDialog} from '@angular/material';
import { Title } from '@angular/platform-browser';

//SERVICES
import { InicioService } from '../../../services/admin/inicio.service';
import { SettingsService } from '../../../services/settings.service';
import { UsuarioService } from '../../../services/admin/usuario.service';
import { PettyInicialService } from '../../../services/admin/petty-inicial.service';
import { PrestamoService } from '../../../services/admin/prestamo.service';

//MODAL
import { PrestamoModalRegistrarComponent } from './modals/prestamo-modal-registrar/prestamo-modal-registrar.component';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-petty-inicial-aperturado',
  templateUrl: './petty-inicial-aperturado.component.html',
  styleUrls: ['./petty-inicial-aperturado.component.scss']
})
export class PettyInicialAperturadoComponent implements OnInit {

  pettyInicialId: number = 0;
  pettyInicial: any = {};
  pettyDetalles: any[] = [];
  prestamos: any[] = [];
  loading: boolean = true;
  vistaPantalla: boolean = false;
  position: string = 'above';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private inicioService: InicioService,
    private settingsService: SettingsService,
    private title: Title,
    private usuarioService: UsuarioService,
    private pettyInicialService: PettyInicialService,
    private prestamoService: PrestamoService,
    public dialog: MatDialog) { }

    ngOnInit() {
      var rolId = parseInt(localStorage.getItem("rolId"));
      if(rolId != 2) this.router.navigate(['sistema/inicio']);
      
      this.title.setTitle("Ver Apertura de Caja");
      this.listarPettyInicialAperturado();
    }

    listarPettyInicialAperturado(){
      this.loading = true;
      let usuarioId = parseInt(localStorage.getItem("id"));
      this.pettyInicialService.listarPettyInicialAperturadoPorUsuario(usuarioId).then((data)=>{
        this.pettyDetalles = data;
        this.pettyInicial.montoBillete = data[0].montoPettyInicial;
        this.pettyInicial.nombreTurno = data[0].nombreTurno;
        this.pettyInicial.createdAt = data[0].createdAt;
        this.pettyInicialId = data[0].id;
        this.listarPrestamoPorPettyInicial();
      });
    }

    listarPrestamoPorPettyInicial(){
      this.prestamoService.listarPrestamoPorPettyInicial(this.pettyInicialId).then((data)=>{
        this.prestamos = [];
        let prestamosTemporal = [];
        for(var i=0; i<data.length; i++){
          prestamosTemporal.push({
            id: data[i].id,
            total: data[i].total,
            descripcion: data[i].descripcion,
            createdAt: data[i].createdAt,
            prestamosDetalle: []
          });
        }
        prestamosTemporal = this.eliminarObjetosDuplicados(prestamosTemporal, "id");

        for(var i=0; i<prestamosTemporal.length; i++){
          for(var j=0; j<data.length; j++){
            if(data[j].id == prestamosTemporal[i].id){
              prestamosTemporal[i].prestamosDetalle.push({
                cantidad: parseInt(data[j].cantidad),
                montoBillete: parseInt(data[j].montoBillete),
                totalBillete: parseInt(data[j].cantidad) * parseInt(data[j].montoBillete),
              });
            }
          }
        }
        this.prestamos = prestamosTemporal;
        this.loading = false;
        this.vistaPantalla = true;
      });
    }

    agregarAperturado(){
      let dialogRef = this.dialog.open(PrestamoModalRegistrarComponent, {
        height: '500px',
        data: { pettyInicialId: this.pettyInicialId },
      });
      dialogRef.afterClosed().subscribe(result => {
        if(result){
          this.listarPettyInicialAperturado();
        }
      });
    }

    cerrarCaja(){
      this.router.navigate(["sistema/petty-final/nuevo/", this.pettyInicialId]);
    }

    eliminarObjetosDuplicados(arr, prop) {
      var nuevoArray = [];
      var lookup  = {};
  
      for (var i in arr) {
        lookup[arr[i][prop]] = arr[i];
      }
  
      for (i in lookup) {
        nuevoArray.push(lookup[i]);
      }
      return nuevoArray;
  }
}
