import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'modulo-petty-inicial',
    template: '<router-outlet></router-outlet>'
})
export class PettyInicialComponent {

    constructor() { }

}
