import { Route, RouterModule } from '@angular/router';
import { PettyInicialComponent } from './petty-inicial.component';
import { ModuleWithProviders } from '@angular/core';

//COMPONENTES
import { PettyInicialRegistrarComponent } from './registrar/petty-inicial-registrar.component';
import { PettyInicialAperturadoComponent } from './aperturado/petty-inicial-aperturado.component';

let routes: Route[] = [
    {
        path: '',
        component: PettyInicialComponent,
        children: [
            { path: 'nuevo',  component: PettyInicialRegistrarComponent },
            { path: 'aperturado',  component: PettyInicialAperturadoComponent },
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
