import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DateAdapter, MatDialog } from '@angular/material';
import { Title } from '@angular/platform-browser';

//SERVICES
import { UsuarioService } from '../../services/admin/usuario.service';
import { SettingsService } from '../../services/settings.service';

@Component({
  selector: 'app-sesion',
  templateUrl: './sesion.component.html',
  styleUrls: ['./sesion.component.css']
})
export class SesionComponent implements OnInit {
  hide = true;
  loading: boolean = false;
  usuario: any = {};

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private usuarioService: UsuarioService,
    private settingsService: SettingsService,
    private title:Title,
  ) { }

  ngOnInit() {
    this.title.setTitle("Iniciar Sesión");
  }

  onSubmit() {
    this.loading = true;
    this.usuarioService.iniciarSesion(this.usuario)
    .then((data)=> {
      this.loading = false;
      if(data.state && (data.rolId == 1 || data.rolId == 2)){
        localStorage.setItem("usuario", data.nombres+' '+data.apellidoPaterno+' '+data.apellidoMaterno);
        localStorage.setItem("id", data.id);
        localStorage.setItem("rolId", data.rolId);
        this.settingsService.showNotification('top','right', "Usuario Correcto", 2);
        this.router.navigate(['sistema/inicio']);
      }else{
        this.settingsService.showNotification('top','right', this.settingsService.mensaje.usuario_error, 4);
      }
    })
    .catch((error) => {
      this.loading = false;
      this.settingsService.showNotification('top','right', this.settingsService.mensaje.usuario_error, 4);
    });
  }
}
