import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

//SERVICES
import { SettingsService } from '../../../services/settings.service';
import { UsuarioService } from '../../../services/admin/usuario.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  bandera: boolean = false;
  nombre_usuario: string;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private settingsService: SettingsService,
    private usuarioService: UsuarioService) { }

  ngOnInit() {
    this.nombre_usuario = localStorage.getItem("usuario");
  }

  reservarCita(){
    this.router.navigate(["reserva-cita"]);
  }

  miCita(){
    this.router.navigate(["mi-cita"]);
  }

  cerrarSesion(){
    this.usuarioService.cerrarSesion().then((data: any) => {
      localStorage.removeItem('jwt');
      localStorage.removeItem('id');
      localStorage.removeItem('usuario');
      this.router.navigate(["iniciar-sesion"]);
    });
  }

}
