import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

//SERVICES
import { SettingsService } from '../../../services/settings.service';

@Component({
  selector: 'app-footer-login',
  templateUrl: './footer-login.component.html',
  styleUrls: ['./footer-login.component.css']
})
export class FooterLoginComponent implements OnInit {

  anio: number;
  fecha_actual = new Date();

  constructor(private router: Router,
    private route: ActivatedRoute,
    private settingsService: SettingsService) { }

  ngOnInit() {
    this.anio = this.fecha_actual.getFullYear();
  }

}
