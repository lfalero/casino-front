import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

//SERVICES
import { SettingsService } from '../../../services/settings.service';
import { UsuarioService } from '../../../services/admin/usuario.service';

@Component({
  selector: 'app-navbar-login',
  templateUrl: './navbar-login.component.html',
  styleUrls: ['./navbar-login.component.css']
})
export class NavbarLoginComponent implements OnInit {

  bandera: boolean = false;
  constructor(private router: Router,
    private route: ActivatedRoute,
    private settingsService: SettingsService,
    private usuarioService: UsuarioService) { }

  ngOnInit() {
    if(localStorage.getItem("id") != null && localStorage.getItem("id") != ''){
      //this.router.navigate(['reserva-cita']);
    }
  }

  cerrarSesion(){
    this.usuarioService.cerrarSesion().then((data: any) => {
      localStorage.removeItem('correo');
      localStorage.removeItem('nombres');
      this.router.navigate(["iniciar-sesion"]);
    });
  }

}
