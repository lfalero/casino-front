import { Route, RouterModule } from '@angular/router';
import { WebComponent } from './web.component';
import { ModuleWithProviders } from '@angular/core';

//COMPONENTES
import { SesionComponent } from './sesion/sesion.component';

let routes: Route[] = [
    {
        path: '',
        component: WebComponent,
        children: [
            { path: 'iniciar-sesion',  component: SesionComponent },
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
