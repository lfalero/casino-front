import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './web.routing';
import {ConfirmationPopoverModule} from 'angular-confirmation-popover';

import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './../app.routes';

//COMPONENTES
import { WebComponent } from './web.component';
import { MatButtonModule, MatRadioModule, MatInputModule, MatMenuModule, MatCheckboxModule, MatNativeDateModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {MatTooltipModule} from '@angular/material/tooltip';

import { NavbarLoginComponent } from './recurso/navbar-login/navbar-login.component'
import { NavbarComponent } from './recurso/navbar/navbar.component'
import { FooterLoginComponent } from './recurso/footer-login/footer-login.component'

//import { SidebarComponent } from './../sidebar/sidebar.component';
//import { NavbarComponent } from './../shared/navbar/navbar.component';

//SERVICES
import { UsuarioService } from '../services/admin/usuario.service';

//COMPONENTES
import { SesionComponent } from './sesion/sesion.component';

//MODALES

//MODALES

@NgModule({
    imports: [
        HttpModule,
        routing,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        MatButtonModule,
        MatRadioModule,
        MatInputModule,
        MatMenuModule,
        MatCheckboxModule,
        MatNativeDateModule,
        MatDatepickerModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        MatTooltipModule,
        ConfirmationPopoverModule.forRoot({
            confirmButtonType: 'danger'
        }),
    ],
    declarations: [
        WebComponent,
        NavbarLoginComponent,
        NavbarComponent,
        FooterLoginComponent,
        SesionComponent,
        //SidebarComponent,
        //NavbarComponent,
    ],
    entryComponents: [

    ],
    providers: [
        UsuarioService,
    ]
})

export class WebModule { }
