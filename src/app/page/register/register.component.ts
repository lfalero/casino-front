import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DateAdapter } from '@angular/material'

//SERVICES
import { SettingsService } from '../../services/settings.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  hide: boolean = true;
  paciente: any = {sexo:0, fecha_nacimiento:"1980-01-01"};
  
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private settingsService: SettingsService,
    private dateAdapter:DateAdapter<Date>) { 
      dateAdapter.setLocale('es'); // DD/MM/YYYY
  }

  ngOnInit() {
    
  }

  onSubmit() {
    /*this.pacienteService.registrar(this.paciente)
    .then((data) => {
      this.settingsService.showNotification('top','right', this.settingsService.mensaje.registrar, 2);
      this.router.navigate(['iniciar-sesion']);
    })
    .catch((error) => {
      this.settingsService.showNotification('top','right', error, 4);
    });*/
    this.settingsService.showNotification('top','right', this.settingsService.mensaje.registrar, 2);
    this.router.navigate(['iniciar-sesion']);
  }

}
