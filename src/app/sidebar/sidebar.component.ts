import {AfterViewInit, Component, OnInit, OnDestroy} from '@angular/core';
import { SettingsService } from '../services/settings.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit, AfterViewInit, OnDestroy {
  public color: string;
  public menuItems: object;
  public activeFontColor: string;
  public normalFontColor: string;
  public dividerBgColor: string;
  constructor(public settingsService: SettingsService) {
    this.activeFontColor = 'rgba(0,0,0,.6)';
    this.normalFontColor = 'rgba(255,255,255,.8)';
    this.dividerBgColor = 'rgba(255, 255, 255, 0.5)';
  }

  ngOnInit() {
    var rolId = parseInt(localStorage.getItem("rolId"));

    if(rolId == 1){
      this.menuItems = [
          { path: 'inicio', title: 'Inicio', icon: 'home', children: null },
          { path: 'maquina', title: 'Máquina', icon: 'work', children: null },
          { path: 'usuario', title: 'Usuario', icon: 'face', children: null },
          { path: '#component', id: 'component', title: 'Reportes', icon: 'timeline', children: [
              {path: 'reporte/pagos-manuales', title: 'Pagos Manuales', icon: ''},
              {path: 'reporte/turno-cerrado', title: 'Turnos Cerrados', icon: ''},
          ]},
      ];
    }else{
      this.menuItems = [
          { path: 'inicio', title: 'Inicio', icon: 'home', children: null },
          { path: 'petty-inicial/nuevo', title: 'Caja', icon: 'payment', children: null },
          { path: 'pago-manual', title: 'Pagos Manuales', icon: 'payment', children: null },
      ];
    }
    
    this.color = this.settingsService.getSidebarFilter();
    this.settingsService.sidebarFilterUpdate.subscribe((filter: string) => {
      this.color = filter;
      if (filter === '#fff') {
        this.activeFontColor = 'rgba(0,0,0,.6)';
      }else {
        this.activeFontColor = 'rgba(255,255,255,.8)';
      }
    });
    this.settingsService.sidebarColorUpdate.subscribe((color: string) => {
      if (color === '#fff') {
        this.normalFontColor = 'rgba(0,0,0,.6)';
        this.dividerBgColor = 'rgba(0,0,0,.1)';
      }else {
        this.normalFontColor = 'rgba(255,255,255,.8)';
        this.dividerBgColor = 'rgba(255, 255, 255, 0.5)';
      }
    });
  }
  ngOnDestroy() {
    this.settingsService.sidebarFilterUpdate.unsubscribe();
    this.settingsService.sidebarColorUpdate.unsubscribe();
  }

  ngAfterViewInit() {
  }
}
